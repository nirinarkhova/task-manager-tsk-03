# TASK MANAGER

# DEVELOPER INFO

NAME: Nadezhda Irinarkhova

E-mail: nirinarkhova@tsconsulting.com

# SOFTWARE

* JDK 1.8

* WINDOWS 10

# HARDVARE

* RAM 16

* CPU I7

* HDD 256

# RUN PROGRAMM

```
java -jar ./task-manager.jar
```

# SCREENSOTS

https://drive.google.com/drive/folders/1qoUIAgvWVYwvqS7BoLZJhoEKoau_VsHk?usp=sharing
